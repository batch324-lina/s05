<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05: activity</title>
</head>
<body>
    <?php session_start(); ?>
    <?php
    
        if(!isset($_SESSION['login'])){
            echo "<form method='POST' action = './server.php'>
            <input type='hidden' name='action' value='login' />
            <label for='email'>Email:</label>
            <input type='email' name='email' required />
            <label for='password'>Password:</label>
            <input type='password' name='password' required />
            <button type='submit'>Login</button>
        </form>";
        } else {
        foreach($_SESSION['login'] as $index => $email){
            echo "<h1>You are now logged in!</h1>
            <p>Hello! $email->email</p>
            <form method='POST' action='./server.php'>
                <input type='hidden' name='action' value='logout' />
                <button type='submit'>Logout</button>
            </form>";
        }
    }
    ?>
</body>
</html>