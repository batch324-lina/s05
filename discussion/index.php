<?php 
	$tasks = ['get git', 'Bake HTML', 'Eat CSS', 'PHP'];

	//$_GET['index'] - it will get the index data from the GET request.
	//isset($_GET['index']) - will wheter the $_GET['index'] variable is existing or not
	if(isset($_GET['index'])){
		//we define indexGet variable and set its value to $_GET['index'] variable.
		$indexGet = $_GET['index'];
		echo "The retrieved task from GET is $tasks[$indexGet]";
	}

	if(isset($_POST['index'])){
		$indexPost = $_POST['index'];
		echo "The retrieved task from the POST is $tasks[$indexPost].";
	}

 ?>

<!DOCTYPE html>
<html>
	<head>
		<title>S05:Client-Server Communication - Discussion 1</title>
	</head>

	<body>
		<h1>Get Method:</h1>
		<form method="GET">
            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>

        </form>

        <h1>POST method:</h1>
        <form method="POST">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">POST</button>

        </form>
	</body>
</html>
